({
    mainConfigFile: '../js/_config.js',

    findNestedDependencies: true,

    removeCombined: true,

    preserveLicenseComments: false,

    optimize: 'none', // 'uglify2',

    optimizeCss: 'none', // 'standard'

    out: '../dist/main.js',

    name: 'app'
})