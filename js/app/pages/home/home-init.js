define([
    'app',
    'marionette',
    'text!template/pages/home/layout.html',
    'pages/home/views/settings',
    'pages/home/views/chart-male',
    'pages/home/views/chart-female',
    'pages/home/views/chart-device',
    'pages/home/views/table-overview'
], function(app, Marionette, template, SettingsView, ChartMale, ChartFemale, ChartDevice, TableOverview) {
    'use strict';

    return Marionette.LayoutView.extend({
        template: template,
        regions: {
            settings: '[data-settings]',
            male: '[data-chart-male]',
            female: '[data-chart-female]',
            device: '[data-chart-device]',
            table: '[data-chart-table]'

        },
        onShow: function() {
            // Settings drop down
            this.settings.show(new SettingsView);

            // Listen for changes on the settings view.
            this.settings.currentView.on('change', function() {
                this.male.currentView.trigger('update');
                this.female.currentView.trigger('update');
                this.device.currentView.trigger('update');
            }, this);

            // Insert home page charts.
            this.male.show(new ChartMale);
            this.female.show(new ChartFemale);
            this.device.show(new ChartDevice);
            this.table.show(new TableOverview);
        }

    });

});
