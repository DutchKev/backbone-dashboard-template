define([
    'app',
    'jquery',
    'marionette',
    'text!template/pages/home/settings.html'
], function(app, $, Marionette, template) {
    'use strict';

    var View = Marionette.ItemView.extend({
        template: template,

        events: {
            'change select': function() {
                this.trigger('change');
            }
        }
    });

    return View;
});
