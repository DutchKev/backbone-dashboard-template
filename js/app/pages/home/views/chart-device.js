define(['components/chart/chart-view'], function(ChartView) {
    return function () {
        return new ChartView({
            title: {
                text: 'Devices'
            }
        });
    }
});