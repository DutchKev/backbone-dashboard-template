define([
    'app',
    'require',
    'marionette',
    'pages/home/home-init',
    'pages/timeline/chart-timeline'
], function(app, require, Marionette, HomePage, TimeLinePage) {
    'use strict';

   var mainRouter = new Marionette.AppRouter({
        controller: {
            home: function() {
                // Trigger the page-switch event, so other components can
                app.vent.trigger('page:switch', 'home');

                // Insert new view.
                app.layout.getRegion('content').show(new HomePage);
            },
            timeLine: function() {
                app.vent.trigger('page:switch', 'timeLine');

                app.layout.getRegion('content').show(new TimeLinePage);
            },
            unknown: function() {
                mainRouter.navigate("home", {trigger: true});
            }
        },

        appRoutes: {
            "": "home",
            home: "home",
            timeLine: "timeLine",
            "*actions" : "unknown"
        }
    });

    return mainRouter
});
