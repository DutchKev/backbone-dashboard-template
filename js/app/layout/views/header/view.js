define([
    'app',
    'marionette',
    'text!template/header.html'
], function (app, Marionette, template) {
    'use strict';

    return Marionette.ItemView.extend({
        template: template,

        initialize: function() {
            app.vent.on('page:switch', this.toggleTab, this);
        },

        toggleTab: function(tabName) {
            this.$el.find('a[href=#' + tabName + ']').tab('show');
        }
    });
});