define([
    'app',
    'marionette',
    'text!template/period-selector.html',
    'jqueryDateRange'
], function(app, Marionette, template) {
    'use strict';

    return Marionette.ItemView.extend({
        template: template,

        onRender: function() {
            this.$rangePicker = this.$el.find('input[data-date-range-selector]');

            this.$rangePicker.daterangepicker(
                {
                    startDate: '01-01-2014',
                    endDate: new Date(),
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                        'Last 7 Days': [moment().subtract('days', 6), moment()],
                        'Last 30 Days': [moment().subtract('days', 29), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                    },
                    showWeekNumbers: true,
                    format: 'DD/MM/YYYY'
                },
                function(start, end, label) {
                    app.vent.trigger('period:change', {start: start, end: end});
                }
            );
        },

        destroy: function() {
            this.$rangePicker.remove();
        }
    });
});
