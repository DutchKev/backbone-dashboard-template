define([
    'app',
    'jquery',
    'marionette',
    'text!template/layout.html',
    'layout/views/period-selector/period-selector',
    'layout/views/header/view',
    'routers/router'
], function (app, $, Marionette, layout, PeriodSelectorView, HeaderView) {
    'use strict';

    app.on('before:start', function() {

        // The 'main-frame' layout (html).
        // This view is rendered instantly, so all regions are available when the app is started.
        app.layout = new Marionette.LayoutView({
            el: 'body',
            template: layout,
            regions: {
                period: '#main-period-selector',
                header: '#header',
                content: '#content'
            }
        }).render();

        // Inject the date-range selector view.
        app.layout.getRegion('period').show(new PeriodSelectorView);

        // Inject the header view.
        app.layout.getRegion('header').show(new HeaderView);

    });

    // Start on document ready. You go girl :)
    $(app.start.bind(app));

    return app;
});