define([
    'marionette',
    'highcharts'
], function (Marionette, Highcharts) {
    'use strict';

    return Marionette.ItemView.extend({
        template: '<div></div>',

        initialize: function() {
            this.on('update', this.update);
        },

        onShow: function() {
            this.buildChart();
        },

        buildChart: function() {
            // Set the current view element as Highcarts root element.
            this.options.chart.renderTo = this.el;

            this.chart = new Highcharts.Chart(this.options);
        },

        update: function() {
            if (!(this.chart instanceof Highcharts.Chart))
                return;

            this.$el.highcharts().series[0].setData([
                ['Android', 50],
                ['Iphone', 50]
            ])
        },

        destroy: function() {
            if (this.chart instanceof Highcharts.Chart) {
                this.chart.destroy();
            }

            this.chart = null;
        },

        // Default chart settings,
        // gets merged automatically by Marionette,
        // with the options that where given when creating new instance.
        options: {
            chart: {
                renderTo: null, // Being set in 'buildChart' function.
                type: 'pie'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: 'black'
                        }
                    }
                }
            },
            series: [{
                name: 'Browser share',
                data: [
                    ['Android', 73.2],
                    ['Iphone', 26.8]
                ]
            }]
        }
    });
});