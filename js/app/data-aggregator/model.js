// Table - Overview
define(['backbone'], function(Backbone) {
    return Backbone.Model.extend({
        defaults: {
            name: "Not specified",
            artist: "Not specified",
            company: "Not specified",
            email: "Not specified"
        }
    });
});
