define(['backbone', 'data-aggregator/model'], function(Backbone, dataModel) {
    return Backbone.Collection.extend({
        url: '/',
        model: dataModel
    });
});

