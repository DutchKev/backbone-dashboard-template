define([
    'backbone',
    'marionette',
    'backboneBBGRID',
    'twitterBootstrap',
    'marionetteTMPL'
], function (Backbone) {
    'use strict';

    // Create a new backbone marionette application.
    var app = new Backbone.Marionette.Application();

    // Start listening for hash changes, when the app starts.
    app.on('start', function() {

        // The 'hashChange' listener.
        Backbone.history.start();

    });

    return app;
});
