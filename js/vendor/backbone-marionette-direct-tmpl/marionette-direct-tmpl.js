define(['marionette'], function(Marionette) {
    Marionette.TemplateCache.prototype.loadTemplate = function() {
        return arguments[0];
    };
});