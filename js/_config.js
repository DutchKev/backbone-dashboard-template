requirejs.config({
    baseUrl: '../js/app',

    paths: {
        // Shortcuts
        app                     : '../_bootstrap', // Returns App instance (the Marionette application).
        template                : '../../template', // Shortcut to templates folder.

        // External libraries
        moment                  : '../vendor/moment/moment.locales', // Date/time converter.
        underscore              : '../vendor/lodash/lodash.2.4.1',
        jquery                  : '../vendor/jquery/jquery.1.11.1.min',
        jqueryDateRange         : '../vendor/jquery-date-range-picker/date.range.picker.1.3.13',
        highcharts              : '../vendor/highcharts/highcharts.4.0.4.min', // Charts library.
        twitterBootstrap        : '../vendor/bootstrap/bootstrap.3.2.0',
        backbone                : '../vendor/backbone/backbone.1.1.2',
        backboneBBGRID          : '../vendor/backbone-bbgrid/bbGrid', // Backbone table view.
        marionette              : '../vendor/backbone-marionette/marionette.2.2.1',
        marionetteTMPL          : '../vendor/backbone-marionette-direct-tmpl/marionette-direct-tmpl',
        text                    : '../vendor/require-text/text.2.0.14' // Template loader
    },

    shim: {
        underscore: {
          exports: '_'
        },
        jquery: {
          exports: 'jQuery'
        },
        jqueryDateRange: {
            deps: ['jquery', 'moment']
        },
        backbone: {
            exports: 'Backbone',
            deps: ['jquery', 'underscore']
        },
        backboneBBGRID: {
            deps: ['backbone', 'jquery']
        },
        marionette: {
            exports : 'Backbone.Marionette',
            deps : ['backbone']
        },
        marionetteTMPL: {
            deps: ['marionette']
        },
        twitterBootstrap : {
            deps :['jquery']
        },
        highcharts: {
            exports: 'Highcharts',
            deps: ['jquery']
        }
    },

    // Load the 'app' file as last, this where the app start signal is given.
    deps: ['../app/app']
});